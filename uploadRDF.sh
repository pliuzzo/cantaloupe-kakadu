#!/bin/sh
# backup can be produced from GUI and downloaded with
# e.g. scp -J pliuzzo@gate.mpcdf.mpg.de pliuzzo@10.186.1.218:/home/pliuzzo/fuseki/apache-jena-fuseki-4.5.0/backups/paths_2024-05-20_05-38-44.nq.gz /Users/pietro/Desktop
#find /backup -type f -name "*.nq" -exec curl --upload-file {} -v -H 'Content-Type: application/n-quads' host.docker.internal:3030/paths/data \;
# ends up in error more often than not
find ../fotothek_access_rdf -type f -name "*.ttl" -exec curl --upload-file {} -v -X POST -H 'Content-Type: text/turtle;charset=utf-8' http://localhost:3030/paths/data \;
