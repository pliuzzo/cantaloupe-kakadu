#!/bin/bash
source ./.env

podman run -d \
 --name frontend \
 --network podman \
 --replace \
 -p 80:80 \
 -v "${NGINX_CONF}":/etc/nginx/conf.d/:rw \
 -v "${NGINX_TEMPLATES}":/etc/nginx/templates/:ro \
 --restart unless-stopped \
 nginx:alpine
