#!/bin/bash
set -euo pipefail

# Check and install crun
if ! command -v crun &> /dev/null; then
   if command -v dnf &> /dev/null; then
       sudo dnf -y install crun
   elif command -v apt &> /dev/null; then
       sudo apt-get update && sudo apt-get -y install crun
   fi
fi

# Check and install jq
if ! command -v jq &> /dev/null; then
   if command -v dnf &> /dev/null; then
       sudo dnf -y install jq
   elif command -v apt &> /dev/null; then
       sudo apt-get -y install jq
   fi
fi

# Check and install podman
if ! command -v podman &> /dev/null; then
   if command -v dnf &> /dev/null; then
       sudo dnf -y install podman
   elif command -v apt &> /dev/null; then
       . /etc/os-release
       echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
       curl -L "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key" | sudo apt-key add -
       sudo apt-get update
       sudo apt-get -y install podman
   fi
fi

# Configure podman rootless
if ! grep -q "$(whoami):100000:65536" /etc/subuid 2>/dev/null; then
   sudo usermod --add-subuids 100000-165535 $(whoami)
fi

if ! grep -q "$(whoami):100000:65536" /etc/subgid 2>/dev/null; then
   sudo usermod --add-subgids 100000-165535 $(whoami)
fi

mkdir -p ~/.config/containers
