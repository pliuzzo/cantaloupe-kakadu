#!/bin/bash

#run all pods
# Load and echo environment variables
if [ -f .env ]; then
   echo "Running pods with environment variables from .env:"
   set -a
   source .env
   set +a
   while IFS='=' read -r key value; do
       if [[ ! $key =~ ^# && -n $key ]]; then
           printf "%s=%s\n" "$key" "${!key}"
       fi
   done < .env
else
   echo "Warning: .env file not found"
fi

./run-nginx.sh
if [ $? -ne 0 ]; then
    echo "Error: run-nginx.sh failed"
    exit 1
fi

./run-fuseki.sh
if [ $? -ne 0 ]; then
    echo "Error: run-fuseki.sh failed"
    exit 1
fi

# we need to get the ip to pass it on to cantaloupe, which should use it in the delegates.rb
FUSEKI_IP=$(podman inspect fusekipaths -f '{{.NetworkSettings.IPAddress}}')
# add ip to .env, so cantaloupe can use it
if grep -q "^FUSEKI_IP=" .env; then
    sed -i "s/^FUSEKI_IP=.*/FUSEKI_IP=$FUSEKI_IP/" .env
else
    echo "FUSEKI_IP=$FUSEKI_IP" >> .env
fi



# add here upload script
if [ "$1" = "upload" ]; then
   ./uploadRDF.sh
   if [ $? -ne 0 ]; then
       echo "Error: upload.sh failed"
       exit 1
   fi
fi

./run-cantaloupe.sh
if [ $? -ne 0 ]; then
    echo "Error: run-cantaloupe.sh failed"
    exit 1
fi

# add here test cantaloupe

# add here cache warm up

echo "All scripts completed successfully"

podman ps

# Check podman network
echo "Checking podman network status:"
podman network ls
podman network inspect podman > /dev/null 2>&1
if [ $? -ne 0 ]; then
   echo "Error: podman network not found"
   exit 1
fi

# Check container IPs
echo -e "\nChecking container IPs:"
podman ps --format "{{.Names}}" | while read -r container; do
   echo "Container: $container"
   podman inspect -f '{{.NetworkSettings.IPAddress}}' "$container"
done


podman exec cantaloupe-podman env | grep FUSEKI
