# cantaloupe-kakadu

This Docker package will start

- Cantaloupe Image server with licenced Kakadu Libraries
- Apache Jena Fuseki instance

Cantaloupe uses delegates.rb to check the Fuseki triplestore for the paths to the images and the

The Apache Jena Fuseki expects a configuration with a text index and backup outside of the container.

PATHS can be changed in .env for deployment. default are local directories in this repository.
