FROM openjdk:17-jdk-alpine

# Cantaloupe docker starter script
# For pyramidal tiffs using JAI processor and filesystem resolver and -cache

# Build and run:
# $ sudo docker build -t cantaloupe .
# $ sudo docker run -d --rm -p 8183:8182 -v ./images:/var/lib/cantaloupe/images --name cantaloupe cantaloupe

ENV CANTALOUPE_VERSION 5.0.6
EXPOSE 8182

RUN apk add --update -X http://dl-cdn.alpinelinux.org/alpine/edge/community \
    curl \
    graphicsmagick

# run non priviledged
#RUN groupadd -r www-data && useradd -r -g www-data cantaloupe
RUN adduser -S cantaloupe

#
# Cantaloupe
#
WORKDIR /tmp
RUN curl -OL https://github.com/cantaloupe-project/cantaloupe/releases/download/v${CANTALOUPE_VERSION}/cantaloupe-${CANTALOUPE_VERSION}.zip
RUN mkdir -p /usr/local/ \
 && cd /usr/local \
 && unzip /tmp/cantaloupe-${CANTALOUPE_VERSION}.zip \
 && ln -s cantaloupe-${CANTALOUPE_VERSION} cantaloupe \
 && rm -rf /tmp/cantaloupe-${CANTALOUPE_VERSION} \
 && rm /tmp/cantaloupe-${CANTALOUPE_VERSION}.zip

# upcoming docker releases: use --chown=cantaloupe
COPY cantaloupe.properties /etc/cantaloupe.properties
COPY delegates.rb /etc/delegates.rb
COPY kakadu-jni/* /usr/lib/
COPY kakadu-jni/* /opt/openjdk-17/lib/
COPY kakadu-jni/* /usr/lib/jni/
COPY kakadu-jni/* /opt/openjdk-17/lib/jni/
RUN mkdir -p /var/log/cantaloupe \
 && mkdir -p /var/cache/cantaloupe \
 && chown -R cantaloupe /var/log/cantaloupe \
 && chown -R cantaloupe /var/cache/cantaloupe \
 && chown cantaloupe /etc/cantaloupe.properties \
 && chown cantaloupe /etc/delegates.rb \
 && chown -R cantaloupe /usr/lib/

USER cantaloupe
#VOLUME ["/var/lib/cantaloupe/images", "/var/log/cantaloupe", "/var/cache/cantaloupe"]
CMD ["sh", "-c", "java -Dcantaloupe.config=/etc/cantaloupe.properties -Xmx2g -jar /usr/local/cantaloupe-${CANTALOUPE_VERSION}/cantaloupe-${CANTALOUPE_VERSION}.jar"]
