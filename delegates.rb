require 'net/http'
require 'json'
require 'java'
require "ipaddr"
require 'uri'
require 'logger'

##
# Sample Ruby delegate script containing stubs and documentation for all
# available delegate methods. See the user manual for more information.
#
# The application will create an instance of this class early in the request
# cycle and dispose of it at the end of the request cycle. Instances don't need
# to be thread-safe, but sharing information across instances (requests)
# **does** need to be done thread-safely.
#
# This version of the script works with Cantaloupe version >= 5.
#

# the container maps /nexus/posix0/MBH-Hertziana to /mnt

# IMPROVEMENT: Added centralized configuration
module ImageConfig
  NEXUS_BASE_PATH = '/mnt/fotothek'.freeze
  FUSEKI_ENDPOINT = "http://#{ENV['FUSEKI_IP']}:3030/paths".freeze
  ALLOWED_IP_RANGE = '193.205.243.0/24'.freeze
  ACCESS_ALLOWED = 'freigegeben'.freeze
  DEFAULT_SMALL_SIZE = 320
  DEFAULT_SMALL_SIZE2 = 450
  DEFAULT_SMALL_SIZE3 = 500
  DEFAULT_LARGE_SIZE = 2048

  # IMPROVEMENT: Organized SPARQL queries with full prefixes
  module Queries
    # Common prefixes used in all queries
    PREFIXES = <<~SPARQL.freeze
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX text: <http://jena.apache.org/text#>
      PREFIX v: <https://gn.biblhertz.it/voc/>
    SPARQL

    # Query for checking both access rights and path
    ACCESS_AND_PATH_QUERY = <<~SPARQL.freeze
      #{PREFIXES}
      SELECT DISTINCT ?path ?access ?filename WHERE {
        ?img1 text:query "*%s*" .
        ?img1 rdfs:label ?filename .
        ?img1 v:path ?path .
        OPTIONAL{?img2 text:query "*%s*" .
                 ?img2 v:access ?accessVal .}

        BIND(COALESCE(?accessVal, "noMidas") AS ?access)
      }
      ORDER BY ?filename ?path
    SPARQL

    # Query that only checks access status
    ACCESS_ONLY_QUERY = <<~SPARQL.freeze
      #{PREFIXES}
      SELECT ?access WHERE {
        ?img text:query "*%s*" .
        ?img v:access ?access .
      }
      LIMIT 1
    SPARQL

    # Query that only looks for path
    PATH_ONLY_QUERY = <<~SPARQL.freeze
      #{PREFIXES}
      SELECT DISTINCT ?path ?filename WHERE {
        ?img text:query "*%s*".
        ?img v:path ?path .
        ?img rdfs:label ?filename .
      }
      ORDER BY ?filename ?path
      LIMIT 1
    SPARQL

    # Query for handling recto variants
    RECTO_PATH_QUERY = <<~SPARQL.freeze
      #{PREFIXES}
      SELECT ?path ?r WHERE {
        {<https://gn.biblhertz.it/%s> v:path ?path .
         BIND("" as ?r)}
        UNION
        {<https://gn.biblhertz.it/%s_recto> v:path ?path .
         BIND("_recto" as ?r)}
      }
    SPARQL

    # Query for finding similar identifiers
    MATCH_ID_QUERY = <<~SPARQL.freeze
      #{PREFIXES}
      SELECT ?id WHERE {
        ?img text:query "*%s*" ;
        BIND(STRAFTER(STR(?img), 'https://gn.biblhertz.it/') AS ?id)
      }
      LIMIT 1
    SPARQL
  end
end

# IMPROVEMENT: Added custom error classes
module DelegateErrors
  class FusekiError < StandardError; end
  class PathNotFoundError < StandardError; end
  class InvalidIdentifierError < StandardError; end
end

class CustomDelegate
  include ImageConfig
  include DelegateErrors

  ##
  # Attribute for the request context, which is a hash containing information
  # about the current request.
  #
  # This attribute will be set by the server before any other methods are
  # called. Methods can access its keys like:
  #
  # ```
  # identifier = context['identifier']
  # ```
  #
  # The hash will contain the following keys in response to all requests:
  #
  # * `client_ip`        [String] Client IP address.
  # * `cookies`          [Hash<String,String>] Hash of cookie name-value pairs.
  # * `full_size`        [Hash<String,Integer>] Hash with `width` and `height`
  #                      keys corresponding to the pixel dimensions of the
  #                      source image.
  # * `identifier`       [String] Image identifier.
  # * `local_uri`        [String] URI seen by the application, which may be
  #                      different from `request_uri` when operating behind a
  #                      reverse-proxy server.
  # * `metadata`         [Hash<String,Object>] Embedded image metadata. Object
  #                      structure varies depending on the source image.
  #                      See the `metadata()` method.
  # * `page_count`       [Integer] Page count.
  # * `page_number`      [Integer] Page number.
  # * `request_headers`  [Hash<String,String>] Hash of header name-value pairs.
  # * `request_uri`      [String] URI requested by the client.
  # * `scale_constraint` [Array<Integer>] Two-element array with scale
  #                      constraint numerator at position 0 and denominator at
  #                      position 1.
  #
  # It will contain the following additional string keys in response to image
  # requests, after the image has been accessed:
  #
  # * `operations`     [Array<Hash<String,Object>>] Array of operations in
  #                    order of application. Only operations that are not
  #                    no-ops will be included. Every hash contains a `class`
  #                    key corresponding to the operation class name, which
  #                    will be one of the `e.i.l.c.operation.Operation`
  #                    implementations.
  # * `output_format`  [String] Output format media (MIME) type.
  # * `resulting_size` [Hash<String,Integer>] Hash with `width` and `height`
  #                    keys corresponding to the pixel dimensions of the
  #                    resulting image after all operations have been applied.
  #
  # @return [Hash] Request context.
  #
  attr_accessor :context

  # IMPROVEMENT: Added structured data type for results
  ImageResult = Struct.new(:path, :access, :label, :filename, :identifier, keyword_init: true)

##
  # Deserializes the given meta-identifier string into a hash of its component
  # parts.
  #
  # This method is used only when the `meta_identifier.transformer`
  # configuration key is set to `DelegateMetaIdentifierTransformer`.
  #
  # The hash contains the following keys:
  #
  # * `identifier`       [String] Required.
  # * `page_number`      [Integer] Optional.
  # * `scale_constraint` [Array<Integer>] Two-element array with scale
  #                      constraint numerator at position 0 and denominator at
  #                      position 1. Optional.
  #
  # @param meta_identifier [String]
  # @return Hash<String,Object> See above. The return value should be
  #                             compatible with the argument to
  #                             {serialize_meta_identifier}.
  #
  def deserialize_meta_identifier(meta_identifier)
  end

  ##
  # Serializes the given meta-identifier hash.
  ##
  # Serializes the given meta-identifier hash.
  #
  # This method is used only when the `meta_identifier.transformer`
  # configuration key is set to `DelegateMetaIdentifierTransformer`.
  #
  # See {deserialize_meta_identifier} for a description of the hash structure.
  #
  # @param components [Hash<String,Object>]
  # @return [String] Serialized meta-identifier compatible with the argument to
  #                  {deserialize_meta_identifier}.
  #
  def serialize_meta_identifier(components)
  end

  ##
  # Returns authorization status for the current request. This method is called
  # upon all requests to all public endpoints early in the request cycle,
  # before the image has been accessed.
  #

def pre_authorize(options = {})
    identifiers = clean_identifier(context['identifier'])
    logger.debug("All headers: #{context['request_headers'].inspect}")
    logger.debug("pre_authorize called for identifier: #{identifiers}")
    logger.debug("Request URI: #{context['request_uri']}")
    logger.debug("Client IP: #{context['request_headers']['X-Real-Ip']}")

    identifiers = [identifiers] unless identifiers.is_a?(Array)
    identifiers = sort_identifiers(identifiers)
    logger.debug("sorted identifiers: #{identifiers}")

    begin

      # Check if this is a tile request (doesn't contain 'full' in the path)
# Check if this is a tile request
       request_uri = context['request_uri']
       is_tile_request = !request_uri.include?('/full/')
       logger.debug("Is tile request: #{is_tile_request}")

    # Allow tile requests to pass through
       if is_tile_request
         logger.debug("Allowing tile request")
         return true
       end

      # Always allow dpub identifiers
      identifiers.each do |identifier|
        if identifier.start_with?('dpub')
          logger.debug("Allowing dpub identifier")
          return true
        end
      end

      # Always allow dpub identifiers
      identifiers.each do |identifier|
        if identifier.start_with?('mss')
          logger.debug("Allowing mss identifier")
          return true
        end
      end

      # Always allow if we're already requesting a small version
      if context['identifier'].to_s.start_with?('#{DEFAULT_SMALL_SIZE.to_s}px/')
         logger.debug("small 320px image requested, go on")
         return true
      end

      # Parse the request URI to check if it's already a restricted size request
      request_uri = context['request_uri']
      if request_uri =~ %r{/[%5E]*!?#{DEFAULT_SMALL_SIZE.to_s},#{DEFAULT_SMALL_SIZE.to_s}/}
        logger.debug("iiif request for #{DEFAULT_SMALL_SIZE.to_s}px image requested, probably a redirect, just move on")
        return true
      end

      # Parse the request URI to check if it's already a restricted size request
      request_uri = context['request_uri']
      if request_uri =~ %r{/[%5E]*!?#{DEFAULT_SMALL_SIZE2.to_s},#{DEFAULT_SMALL_SIZE2.to_s}/}
        logger.debug("iiif request for #{DEFAULT_SMALL_SIZE2.to_s}px image requested, probably a redirect, just move on")
        return true
      end
      
      # Parse the request URI to check if it's already a restricted size request
      request_uri = context['request_uri']
      if request_uri =~ %r{/[%5E]*!?#{DEFAULT_SMALL_SIZE3.to_s},#{DEFAULT_SMALL_SIZE3.to_s}/}
        logger.debug("iiif request for #{DEFAULT_SMALL_SIZE3.to_s}px image requested, probably a redirect, just move on")
        return true
      end

      # For institutional IPs, immediately allow access
      if valid_institutional_ip?
        logger.debug("Allowing institutional IP access")
        return true
      end

      identifiers.each do |identifier|
        logger.debug("Checking Fuseki for access rights")
        # For external requests, check access rights
        response = query_fuseki(build_query(:access_only, identifier))
        result = parse_fuseki_response(response)


        if result&.access == ACCESS_ALLOWED
           logger.debug("Access is allowed by Fuseki")
           return  true
        end
      end

    # If none of the variants are allowed, fallback to small image
          logger.debug("Access not allowed, redirecting to small version")
          fallback_to_small_image(identifiers.first)

    rescue StandardError => e
        logger.debug("Access not allowed, redirecting to small version")
        fallback_to_small_image(identifiers.first)
    end
  end
##
  # Returns authorization status for the current request. Will be called upon
  # all requests to all public image (not information) endpoints.
  #
  # This is a counterpart of `pre_authorize()` that is invoked later in the
  # request cycle, once more information about the underlying image has become
  # available. It should only contain logic that depends on context keys that
  # contain information about the source image (like `full_size`, `metadata`,
  # etc.)
  #
  # @param options [Hash] Empty hash.
  # @return [Boolean,Hash<String,Object>] See the documentation of
  #                                       `pre_authorize()`.
  #
def authorize(options = {})
    # Simply return true here to avoid double-checking
    # pre_authorize has already handled the access control
    true
  end

  ##
  # Tells the server which source to use for the given identifier.
  #
  # @param options [Hash] Empty hash.
  # @return [String] Source name.
  #
  def source(options = {})
  end

def sort_identifiers(identifiers)
    # Convert to array if it's a single string
    identifiers = [identifiers] unless identifiers.is_a?(Array)

    identifiers.sort do |a, b|
      if a.upcase == a && b.upcase != b
        -1  # Capitalized comes first
      elsif a.upcase != a && b.upcase == b
        1   # Non-capitalized comes second
      else
        a.length <=> b.length  # Shorter comes first
      end
    end
  end

  ##
  # N.B.: this method should not try to perform authorization. `authorize()`
  # should be used instead.
  #
  # @param options [Hash] Empty hash.
  # @return [String,nil] Absolute pathname of the image corresponding to the
  #                      given identifier, or nil if not found.
  #

def filesystemsource_pathname(options = {})
  identifiers = clean_identifier(context['identifier'])
  # Ensure we have an array of variants
  identifiers = [identifiers] unless identifiers.is_a?(Array)
  identifiers = sort_identifiers(identifiers)
  logger.debug("sorted identifiers: #{identifiers}")

  # Return early if no valid identifiers
  return nil if identifiers.empty? || identifiers.all?(&:nil?)

  primary_paths = []
  fallback_paths = []

  begin
    # Try each identifier variant
    identifiers.each do |identifier|
      next if identifier.nil? || identifier.empty?
      # Always handle dpub first
      if identifier.start_with?('dpub')
        begin
          logger.debug("Digital Publishing Identifier")
          path = handle_dpub(identifier)
            if path && File.exist?(path)
               primary_paths << PathResult.new(path: path, type: :primary, identifier: identifier)
            end
          next
        rescue PathNotFoundError => e
          logger.debug("Digital Publishing path not found: #{e.message}")
          # Continue to next variant if dpub fails
          next
        end
      end

      if identifier.start_with?('mss')
        begin
          logger.debug("Mapping Sacred Spaces Identifier")
          path = handle_mss(identifier)
            if path && File.exist?(path)
               primary_paths << PathResult.new(path: path, type: :primary, identifier: identifier)
            end
          next
        rescue PathNotFoundError => e
          logger.debug("Mapping Sacred Spaces path not found: #{e.message}")
          # Continue to next variant if dpub fails
          next
        end
      end


      # Path resolution for different access scenarios
      path_result = if valid_institutional_ip?
        # For institutional IPs, just resolve path without access check
        logger.debug("resolving for institutional IP: #{identifier}")
        resolve_path_only(identifier)
      else
        # For external access, we already checked access in pre_authorize
        logger.debug("resolving for external access: #{identifier}")
        resolve_external_path(identifier)
      end

      # Only process and add non-nil results
      if path_result && path_result.respond_to?(:primary?)
        begin
          if path_result.primary?
            logger.debug("Added primary path: #{path_result.path}")
            primary_paths << path_result if path_result.path
          else
            logger.debug("Added fallback path: #{path_result.path}")
            fallback_paths << path_result if path_result.path
          end
        rescue NoMethodError => e
          logger.error("Error processing path result: #{e.message}")
          # Continue to next identifier
          next
        end
      else
        logger.debug("Skipping nil or invalid path result for identifier: #{identifier}")
      end

    end

    logger.debug("added all possible primary and fallback paths")

     # Log all found paths
    logger.debug("\nAll found paths:")
    logger.debug("Primary paths:")
    primary_paths.each { |p| logger.debug("  - #{p.path} (#{p.identifier})") }
    logger.debug("Fallback paths:")
    fallback_paths.each { |p| logger.debug("  - #{p.path} (#{p.identifier})") }

    # Process paths after logging
    logger.debug("Processing paths for return...")

    # First try primary paths
    if primary_paths.any?
      primary_paths.each do |path_result|
        next unless path_result
        begin
          if path_result.path && File.exist?(path_result.path)
            logger.debug("Returning valid primary path: #{path_result.path}")
            return path_result.path.to_s
          end
        rescue NoMethodError => e
          logger.error("Error accessing primary path: #{e.message}")
          next
        end
      end
    end

    # Then try fallback paths
    if fallback_paths.any?
      fallback_paths.each do |path_result|
        next unless path_result
        begin
          if path_result.path && File.exist?(path_result.path)
            logger.debug("Returning valid fallback path: #{path_result.path}")
            return path_result.path.to_s
          end
        rescue NoMethodError => e
          logger.error("Error accessing fallback path: #{e.message}")
          next
        end
      end

      # If we have fallback paths but none were valid, try the first one's path directly
      if fallback_paths.first && fallback_paths.first.respond_to?(:path)
        path_str = fallback_paths.first.path.to_s
        if !path_str.empty? && File.exist?(path_str)
          logger.debug("Returning first fallback path directly: #{path_str}")
          return path_str
        end
      end
    end

    # Last resort: try 320px path directly
    logger.debug("Trying 320px paths as last resort...")
    identifiers.each do |identifier|
      next unless identifier
      begin
        path_320 = File.join(NEXUS_BASE_PATH, '320px', "#{identifier}.jpg")
        if File.exist?(path_320)
          logger.debug("Returning valid 320px path: #{path_320}")
          return path_320.to_s
        end

        # Also try with _recto suffix
        path_320_recto = File.join(NEXUS_BASE_PATH, '320px', "#{identifier}_recto.jpg")
        if File.exist?(path_320_recto)
          logger.debug("Returning valid 320px recto path: #{path_320_recto}")
          return path_320_recto.to_s
        end
      rescue StandardError => e
        logger.error("Error processing 320px path for #{identifier}: #{e.message}")
        next
      end
    end

    # If still no path found, log error and return nil explicitly
    logger.error("No valid path found for identifiers: #{identifiers}")
    nil  # Explicit nil return

  rescue PathNotFoundError => e
    logger.error("Path resolution failed: #{e.message}")
    nil
  end
end

private

  def logger
    @logger ||= Java::edu.illinois.library.cantaloupe.delegate.Logger
  end

def clean_identifier(identifier)
  return '' if identifier.nil?
  return identifier if identifier.start_with?('dpub')
  return identifier if identifier.start_with?('mss')

  id = identifier.gsub(/\.\./, '').strip
  base = File.basename(id, '.*')
  variants = []

  if ['bhp', 'bhf', 'bhon', 'bhdia'].any? { |prefix| base.start_with?(prefix) }
    variants << base
  elsif base.start_with?('GERN_')
    variants << base
    variants << "bh_GERN_#{base.sub('GERN_', '')}"
    variants << "bh_gern_#{base.sub('GERN_', '')}"
  elsif base.start_with?('bh_gern_')
    variants << base
    variants << base.gsub('bh_gern_', 'bh_GERN_')
    variants << "GERN_#{base.sub('bh_gern_', '')}"
  elsif base.start_with?('bh_GERN_')
    variants << base
    variants << base.gsub('bh_GERN_', 'bh_gern_')
    variants << "GERN_#{base.sub('bh_GERN_', '')}"
  else
    variants << base
    variants << "#{base}_recto"
  end

  variants.uniq
end

#  def valid_institutional_ip?
#    return false unless context['request_headers']

#    real_ip = context['request_headers']['X-Real-IP']
#    forwarded_ip = context['request_headers']['X-Forwarded-For']
#    logger.debug("Real IP: #{real_ip}")
#    logger.debug("Forwarded IP: #{forwarded_ip}")
#    return false unless real_ip
#
#    begin
#      client_ip = IPAddr.new(real_ip)
#      logger.debug("Validation - Client IP: #{client_ip}")
#      institution_range = IPAddr.new(ALLOWED_IP_RANGE)
#      institution_range.include?(client_ip)
#    rescue IPAddr::InvalidAddressError => e
#      logger.error("Invalid IP address: #{e.message}")
#      false
#    end
#  end

def valid_institutional_ip?
  return false unless context['request_headers']
  real_ip = context['request_headers']['X-Real-IP']
  forwarded_ips = context['request_headers']['X-Forwarded-For']&.split(',')&.map(&:strip)
  logger.debug("Forwarded IPs: #{forwarded_ips}")
  client_ip_str = forwarded_ips&.first || real_ip
  logger.debug("all IPs: #{client_ip_str}")

  return false unless client_ip_str

  begin
    client_ip = IPAddr.new(client_ip_str)
    logger.debug("Client IP: #{client_ip}")
    logger.debug("Validation - Client IP: #{client_ip}")
    institution_range = IPAddr.new(ALLOWED_IP_RANGE)
    institution_range.include?(client_ip)
  rescue IPAddr::InvalidAddressError => e
    logger.error("Invalid IP address: #{e.message}")
    false
  end
end

def small_image_request?
    # Check if we're already requesting a 320px version
    return true if context['identifier'].to_s.start_with?("#{DEFAULT_SMALL_SIZE.to_s}px/")

    # Or if the resulting size will be small
    resulting_size = context['resulting_size']
    return false unless resulting_size

    resulting_size['width'] <= DEFAULT_SMALL_SIZE ||
    resulting_size['height'] <= DEFAULT_SMALL_SIZE
  end


  def check_external_access(identifier)
    response = query_fuseki(build_query(:access_only, identifier))
    result = parse_fuseki_response(response)

    if result&.access == ACCESS_ALLOWED
      true
    else
      fallback_to_small_image(identifier)
    end
  end

  def query_fuseki(query)
    logger.debug("sending query to #{FUSEKI_ENDPOINT}")
    uri = URI("#{FUSEKI_ENDPOINT}/query")
    uri.query = URI.encode_www_form(
      format: 'json',
      query: query
    )

    response = Net::HTTP.get_response(uri)
    raise FusekiError, "Query failed: #{response.code}" unless response.is_a?(Net::HTTPSuccess)

    JSON.parse(response.body)
  rescue StandardError => e
    raise FusekiError, "Fuseki query failed: #{e.message}"
  end

  def build_query(type, identifier)
    case type
    when :access_and_path
      format(Queries::ACCESS_AND_PATH_QUERY, identifier, identifier)
    when :access_only
      format(Queries::ACCESS_ONLY_QUERY, identifier)
    when :path_only
      format(Queries::PATH_ONLY_QUERY, identifier)
    when :recto_path
      format(Queries::RECTO_PATH_QUERY, identifier, identifier)
    when :match_id
      format(Queries::MATCH_ID_QUERY, identifier)
    else
      raise ArgumentError, "Unknown query type: #{type}"
    end
  end

  def parse_fuseki_response(response)
    result = response.dig('results', 'bindings')&.first
    return nil if result.nil?

    ImageResult.new(
      path: result.dig('path', 'value'),
      access: result.dig('access', 'value'),
      label: result.dig('label', 'value'),
      filename: result.dig('filename', 'value'),
      identifier: result['id']&.dig('value') || context['identifier']
    )
  rescue StandardError => e
    # logger.error("Failed to parse Fuseki response: #{e.message}")
    nil
  end


# Define a custom result class to distinguish path types
class PathResult
  attr_reader :path, :type, :identifier

  def initialize(path:, type: :primary, identifier: nil)
    @path = path
    @type = type
    @identifier = identifier
  end

  def primary?
    type == :primary
  end

  def fallback?
    type == :fallback
  end

  def to_s
    @path.to_s
  end
end

def resolve_external_path(identifier)
  logger.debug("Checking external path for identifier: #{identifier}")

  begin
    # First, try access and path query
    response = query_fuseki(build_query(:access_and_path, identifier))
    result = parse_fuseki_response(response)

    logger.debug("Fuseki result for #{identifier}: #{result.inspect}")

    # Skip if no path or result is nil
    if result.nil? || result.path.nil?
      logger.debug("No valid result found for #{identifier}")
      return nil
    end

    # Construct full path
    path = construct_full_path(result, identifier)
    logger.debug("Constructed path: #{path}")

    # Check file existence
    if File.exist?(path)
      # Determine path type based on access
      path_type = result.access == ACCESS_ALLOWED ? :primary : :fallback

      return PathResult.new(
        path: path,
        type: path_type,
        identifier: identifier
      )
    else
      logger.debug("Path does not exist: #{path}")
      return nil
    end

  rescue StandardError => e
    logger.error("Error processing identifier #{identifier}: #{e.message}")
  end

  # Last resort fallback
  fallback_path = resolve_fallback_path(identifier)
  PathResult.new(path: fallback_path, type: :fallback, identifier: identifier)
end

def resolve_path_only(identifier)
  logger.debug("Checking path for identifier: #{identifier}")

  begin
    # Store result in a variable
    result = nil  # Initialize the variable first

    # Try path-only query
    response = query_fuseki(build_query(:path_only, identifier))
    #result = parse_fuseki_response(response)

    logger.debug("Path result for #{identifier}: #{result.inspect}")

    # First let's verify the query construction
    query = build_query(:path_only, identifier)
    logger.debug("Generated SPARQL query: #{query}")

    # Let's add more detailed response logging
    response = query_fuseki(query)
    logger.debug("Raw Fuseki response: #{response.inspect}")

    # Check the bindings directly
    bindings = response.dig('results', 'bindings')
    logger.debug("Bindings: #{bindings.inspect}")

    result = parse_fuseki_response(response)
    logger.debug("Parsed result: #{result.inspect}")


    # Skip if no path found
    return nil unless result&.path

    # Construct full path
    path = construct_full_path(result, identifier)
    logger.debug("Constructed path: #{path}")

    # Check file existence and return primary path
    if File.exist?(path)
      logger.debug("Found existing path: #{path}")
      return PathResult.new(
        path: path,
        type: :primary,
        identifier: identifier
      )
      else
      logger.debug("Path does not exist: #{path}")
      return nil  # Return nil instead of trying fallback here
    end
  rescue StandardError => e
    logger.error("Error processing identifier #{identifier}: #{e.message}")
    nil
  end

  # Last resort fallback
  fallback_path = resolve_fallback_path(identifier)
  PathResult.new(path: fallback_path, type: :fallback, identifier: identifier)
end

def resolve_fallback_path(identifier)
  return nil if identifier.nil? || identifier.empty?
  logger.debug("Trying fallback path for: #{identifier}")

  # Try each variant
  variants = if identifier.upcase.include?('GERN')
    base = identifier.sub(/^bh_/, '')
    ["bh_#{base.upcase}", base.upcase, "bh_#{base}", base]
  else
    [identifier]
  end

  variants.each do |variant|
    next if variant.nil? || variant.empty?

    # Try various paths
    paths_to_try = []

    if valid_institutional_ip?
      paths_to_try << File.join(NEXUS_BASE_PATH, 'IIIF/Fotothek', "#{variant}.jp2")
      paths_to_try << File.join(NEXUS_BASE_PATH, '2048px', "#{variant}.jpg")
    end

    ['', '_recto'].each do |suffix|
      paths_to_try << File.join(NEXUS_BASE_PATH, '320px', "#{variant}#{suffix}.jpg")
    end

    # Try each path
    paths_to_try.each do |path|
      if File.exist?(path)
        logger.debug("Found existing path: #{path}")
        return path
      end
    end

    # Try Fuseki as last resort
    begin
      response = query_fuseki(build_query(:path_only, variant))
      result = parse_fuseki_response(response)
      if result&.path
        path = construct_full_path(result, variant)
        if File.exist?(path)
          logger.debug("Found path from Fuseki: #{path}")
          return path
        end
      end
    rescue StandardError => e
      logger.error("Fuseki query failed for #{variant}: #{e.message}")
      next
    end
  end

  logger.error("No fallback path found for: #{identifier}")
  nil
end

def construct_full_path(result, identifier)
  # Normalize the base path
  logger.debug("Received result path: #{result.path.to_s}")
  base_path = result.path.start_with?('/nexus/posix0') ?
              result.path :
              File.join(NEXUS_BASE_PATH, 'IIIF/Fotothek', result.path)
  logger.debug("before Directory mapping path: #{base_path}")
  # Define directory mappings
  directory_mappings = {
    'ROMA_CHIESE' => 'Roma_Chiese/drawers',
    'ROMA_ANTICHITA' => 'Roma_Antichita/boxes',
    'ROMA_CIMITERI' => 'Roma_Cimiteri/boxes',
    'ROMA_MUSEI_ASTE' => 'Roma_Musei_Aste/boxes',
    'ROMA_PALAZZI' => 'Roma_Palazzi/boxes',
    'ROMA_URBANISTICA' => 'Roma_Urbanistica/boxes',
    'ROMA_VILLE' => 'Roma_Ville/boxes'
  }

  # Apply directory mappings
  directory_mappings.each do |old_path, new_path|
    base_path.gsub!(old_path, new_path)
  end


  # Clean up path
  base_path = base_path.gsub('//', '/').gsub('/./', '/')

  # Use result.filename if it exists and is not empty, otherwise use identifier
  file_name = if result.respond_to?(:filename) && !result.filename.to_s.strip.empty?
                logger.debug("Using result.filename: #{result.filename}")
                "#{result.filename}.jp2"
              else
                logger.debug("Using identifier: #{identifier}")
                "#{identifier}.jp2"
              end

  # Construct full path
  full_path = File.join(base_path, file_name)

  logger.debug("Constructed full path: #{full_path}")

  full_path
end


  def fallback_to_small_image(identifier)
    {
      'status_code' => 303,
      'location' => "/iiif/3/#{identifier}.jpg/full/^!#{DEFAULT_SMALL_SIZE},#{DEFAULT_SMALL_SIZE}/0/default.jpg"
    }
  end

#def handle_dpub(identifier)
#  logger.debug("handle_dpub called with identifier: #{identifier}")
#  path = File.join(NEXUS_BASE_PATH, identifier)
#  logger.debug("Attempting path: #{path}")
#  return path if File.exist?(path)
#
#  raise PathNotFoundError, "DPUB image not found: #{path}"
#end

  def handle_dpub(identifier)
    logger.debug("handle_dpub called with identifier: #{identifier}")
    path = File.join(NEXUS_BASE_PATH, '/',identifier)
    logger.debug("Attempting path: #{path}")
    return path if File.exist?(path)

    raise PathNotFoundError, "Digital Publishing image not found: #{path}"
  end

  def handle_mss(identifier)
    logger.debug("handle_mss called with identifier: #{identifier}")
    path = File.join(NEXUS_BASE_PATH, '/',identifier)
    logger.debug("Attempting path: #{path}")
    return path if File.exist?(path)

    raise PathNotFoundError, "Mapping Sacred Spaces image not found: #{path}"
  end

  ##
  # Tells the server what overlay, if any, to apply to an image. Called upon
  # all image requests to any endpoint if overlays are enabled and the overlay
  # strategy is set to `ScriptStrategy` in the application configuration.
  #
  def overlay(options = {})
    return nil if context['operations']&.find { |op| op['class'].include?('Crop') }

    resulting_size = context['resulting_size']
    return nil unless resulting_size
    return nil if resulting_size['width'] < DEFAULT_SMALL_SIZE ||
                 resulting_size['height'] < DEFAULT_SMALL_SIZE

    {
      'image' => File.join(NEXUS_BASE_PATH, 'logo/bhmpismall.png'),
      'position' => 'top left',
      'inset' => 10
    }
  end

  ##
  # Returns metadata to embed in the derivative image.
  #
 ##
  # Tells the server what regions of an image to redact in response to a
  # particular request. Will be called upon all image requests to any endpoint.
  #
  # @param options [Hash] Empty hash.
  # @return [Array<Hash<String,Integer>>] Array of hashes, each with `x`, `y`,
  #         `width`, and `height` keys; or an empty array if no redactions are
  #         to be applied.
  #
  def redactions(options = {})
    []
  end
   ##
  # Returns XMP metadata to embed in the derivative image.
  #
  # Source image metadata is available in the `metadata` context key, and has
  # the following structure:
  #
  # ```
  # {
  #     "exif": {
  #         "tagSet": "Baseline TIFF",
  #         "fields": {
  #             "Field1Name": value,
  #             "Field2Name": value,
  #             "EXIFIFD": {
  #                 "tagSet": "EXIF",
  #                 "fields": {
  #                     "Field1Name": value,
  #                     "Field2Name": value
  #                 }
  #             }
  #         }
  #     },
  #     "iptc": [
  #         "Field1Name": value,
  #         "Field2Name": value
  #     ],
  #     "xmp_string": "<rdf:RDF>...</rdf:RDF>",
  #     "xmp_model": https://jena.apache.org/documentation/javadoc/jena/org/apache/jena/rdf/model/Model.html
  #     "native": {
  #         # structure varies
  #     }
  # }
 #
  # * The `exif` key refers to embedded EXIF data. This also includes IFD0
  #   metadata from source TIFFs, whether or not an EXIF IFD is present.
  # * The `iptc` key refers to embedded IPTC IIM data.
  # * The `xmp_string` key refers to raw embedded XMP data, which may or may
  #   not contain EXIF and/or IPTC information.
  # * The `xmp_model` key contains a Jena Model object pre-loaded with the
  #   contents of `xmp_string`.
  # * The `native` key refers to format-specific metadata.
  #
  # Any combination of the above keys may be present or missing depending on
  # what is available in a particular source image.
  #
  # Only XMP can be embedded in derivative images. See the user manual for
  # examples of working with the XMP model programmatically.
  #
  # @return [String,Model,nil] String or Jena model containing XMP data to
  #                            embed in the derivative image, or nil to not
  #                            embed anything.
  #
  def metadata(options = {})
     context.dig('metadata', 'xmp_string') if context && context['metadata']
  end

  ##
  # Adds additional keys to an Image API information response.
  #
  def extra_information_response_keys
    {
      'xmp' => context.dig('metadata', 'xmp_string')
    } if context && context['metadata']
  end
end
