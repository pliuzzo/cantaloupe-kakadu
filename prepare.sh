#!/bin/sh

sync_repo() {
    local repo_url="$1"
    local repo_name="$2"

    # Validate input
    if [ -z "$repo_url" ] || [ -z "$repo_name" ]; then
        echo "Error: Both repository URL and repository name must be provided."
        return 1
    fi

    # Get the directory of the script
    local script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    # Target directory at the same level as the script's directory
    local target_dir="$( dirname "$script_dir" )/$repo_name"

    # Check if directory doesn't exist, then clone
    if [ ! -d "$target_dir" ]; then
        git clone "$repo_url" "$target_dir"
        echo "Repository cloned to $target_dir"
    else
        # If directory exists, pull latest changes
        cd "$target_dir" && git pull
        echo "Pulled latest changes in $target_dir"
    fi
}

echo 'clone cantaloupe_kakadu_public with Alexander Drummer kakadu integration in docker file'
sync_repo https://gitlab.mpcdf.mpg.de/adrum/cantaloupe_kakadu_public.git cantaloupe_kakadu_public


echo 'clone licenced kakadu'
sync_repo https://gitlab.mpcdf.mpg.de/pliuzzo/kakadu.git kakadu

echo 'copy licenced kakadu where expected by cantaloupe_kakadu_public'
cp kakadu/* ../cantaloupe_kakadu_public/kakadu*

echo 'clone rdf data repository'
sync_repo  https://gitlab.mpcdf.mpg.de/pliuzzo/fotothek_access_rdf.git fotothek_access_rdf

echo 'build podman images'
./build-cantaloupe.sh
./build-fuseki.sh

echo 'repositories cloned, images built, you can proceed with run.sh'


