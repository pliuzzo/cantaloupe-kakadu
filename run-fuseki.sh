#!/bin/bash
# Load environment variables

source ./.env

podman run -d \
  --name fusekipaths \
  --network podman \
  --platform linux/amd64 \
  --replace \
  -e JAVA_OPTIONS="-Xmx8g -Xms4g -Dlog4j.configurationFile=/fuseki/log4j2.properties" \
  -p 3030:3030 \
  -v "${FUSEKI_PATHS}":/fuseki/paths:rw \
  -v "${FUSEKI_LUCENE}":/fuseki/lucene:rw \
  -v "${FUSEKI_DATABASE}":/fuseki/databases:rw \
  -v "${FUSEKI_CONFIGURATION}":/fuseki/configuration:rw \
  --restart unless-stopped \
  localhost/fuseki/fotoserver:latest --conf /fuseki/configuration/paths.ttl

# Check permissions and ownership

echo "Checking permissions..."

podman exec fusekipaths ls -la /fuseki/
podman exec fusekipaths id
podman exec fusekipaths ls -la /fuseki/configuration/paths.ttl
