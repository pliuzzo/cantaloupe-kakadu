#!/bin/bash

source ./.env
# Configuration
FUSEKI_ENDPOINT="https://hertz-foto-os3.biblhertz.it/fuseki/paths3/query"
CANTALOUPE_URL="https://hertz-foto-os3.biblhertz.it/iiif/3"
BATCH_SIZE=1000
LOG_DIR="cache_logs/$(date +%Y%m%d_%H%M%S)"

# Parse sizes argument into array and format for IIIF
# Parse sizes
declare -a SIZES
if [ "$2" ]; then
    IFS=',' read -ra tmp_sizes <<< "$2"
    for size in "${tmp_sizes[@]}"; do
        SIZES+=("!$size,$size")
    done
else
    SIZES=("!500,500" "!2048,2048")
fi

shift $(($OPTIND - 1))
while getopts "tr" opt; do
    case $opt in
        t) test_mode=true ;;
        r) random_mode=true ;;
        *) echo "Usage: $0 [-t] [-r]"; exit 1 ;;
    esac
done

mkdir -p "$LOG_DIR"
MAIN_LOG="$LOG_DIR/main.log"
ERROR_LOG="$LOG_DIR/errors.log"
SUCCESS_LOG="$LOG_DIR/success.log"

# SPARQL query template with LIMIT and OFFSET
read -r -d '' QUERY << 'ENDQUERY'
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX v: <https://gn.biblhertz.it/voc/>
SELECT DISTINCT ?image
WHERE {
  ?s rdfs:label ?image ;
      v:access ?access .
}
LIMIT %d
OFFSET %d
ENDQUERY

read -r -d '' RANDOM_QUERY << 'ENDQUERY'
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX v: <https://gn.biblhertz.it/voc/>
SELECT DISTINCT ?image
WHERE {
  ?s rdfs:label ?image ;
      v:access ?access .
     }
ORDER BY RAND()
LIMIT 100
ENDQUERY


log() {
    local msg="$(date '+%Y-%m-%d %H:%M:%S') - $1"
    echo "$msg" >> "$MAIN_LOG"
    echo "$msg"
}

error_log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "$ERROR_LOG"
}

success_log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "$SUCCESS_LOG"
}

# Query Fuseki and log response
query_fuseki() {
    local query="$1"
    local response_file="$LOG_DIR/fuseki_response_$(date +%s).log"

    curl -v -H "Accept: text/tab-separated-values" \
        --data-urlencode "query=${query}" \
        "${FUSEKI_ENDPOINT}" 2> "$response_file" |
        # Skip header row and empty lines
        awk 'NF && !/^\?image/'

    log "Fuseki response logged to: $response_file"
}

check_cache() {
    local url="$1"
    local image_id="$2"
    local size="$3"

    echo "URL: $url"
    echo -n "Testing $image_id ($size)... "

    # First request - should cache the image
    local start1=$(date +%s%3)
    local resp1=$(curl -s -L -w "%{http_code}" -o /dev/null "$url")
    local currentt1=$(date +%s%3)
    local time1=$((currentt1 - start1))

    if [ "$resp1" -ne 200 ]; then
        echo "Failed (HTTP $resp1)"
        error_log "Failed to cache: $image_id (${size}) - HTTP $resp1"
        return 1
    fi

    echo 'wait half sec before sending next request'
    sleep 0.500

    # Second request - should be faster if cached
    local start2=$(date +%s%3)
    local resp2=$(curl -s -L -w "%{http_code}" -o /dev/null "$url")
    local currentt2=$(date +%s%3)
    local time2=$(((currentt2 - start2)))

    if [ "$resp2" -ne 200 ]; then
        echo "Failed verification"
        error_log "Cache verification failed: $image_id (${size}) - HTTP $resp2"
        return 1
    fi

    # Verify second request was significantly faster
    if [ -n "$time2" ] && [ -n "$time1" ] && [ "$time2" -le "$time1" ]; then
        echo "Cached (${time1}ms → ${time2}ms)"
        success_log "Cached and verified: $image_id (${size}) - Initial: ${time1}ms, Cached: ${time2}ms"
        return 0
    else
        echo "Not cached (${time1}ms → ${time2}ms)"
        error_log "Cache performance check failed: $image_id (${size}) - Initial: ${time1}ms, Cached: ${time2}ms"
        return 1
    fi
}

process_images() {
    local images="$1"

    while IFS= read -r image; do
        [ -z "$image" ] && continue
        image=$(echo "$image" | tr -d '"')
        for size in "${SIZES[@]}"; do
            local url="${CANTALOUPE_URL}/${image}/full/${size}/0/default.jpg"
            check_cache "$url" "$image" "$size"
        done
    done <<< "$images"
}

process_batch() {
    local offset="$1"
    local limit="$2"

    local query=$(printf "$QUERY" "$limit" "$offset")
    local images=$(query_fuseki "$query")

    [ -z "$images" ] && return 1
    process_images "$images"
    return 0
}

main() {
    log "Starting cache warmup"

    if [ "$random_mode" = true ]; then
        log "Random mode: processing 100 random images"
        local images=$(query_fuseki "${RANDOM_QUERY}")
        process_images "$images"
    else
        local offset=0
        local limit=$BATCH_SIZE
        [ "$test_mode" = true ] && limit=100

        while true; do
            process_batch "$offset" "$limit" || break
            [ "$test_mode" = true ] && break
            offset=$((offset + limit))
        done
    fi

    log "Cache warmup complete"
}

main "$@"
