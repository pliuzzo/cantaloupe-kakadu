#!/bin/bash

# Script to run Cantaloupe image server in a Podman container
# Mounts directories for images, logs, and cache
# Maps container port 8182 to host port 80

#sudo sysctl net.ipv4.ip_unprivileged_port_start=80

# Load environment variables
source ./.env

podman run -d \
    --replace \
    --network podman \
    --name cantaloupe-podman \
    -p 8182:8182 \
    -e JAVA_OPTIONS="-Xmx8g -Xms4g" \
    -e FUSEKI_IP=${FUSEKI_IP} \
    -v ${NEXUS}:/mnt \
    -v ${CANTALOUPE_LOGS_DIR}:/var/log/cantaloupe \
    -v ${CANTALOUPE_CACHE_DIR}:/var/cache/cantaloupe \
    -v $PWD/cantaloupe.properties:/etc/cantaloupe.properties \
    -v $PWD/delegates.rb:/etc/delegates.rb \
    --restart=unless-stopped \
    localhost/drummerroma/cantaloupe
